// Import the rclcpp client library
#include "rclcpp/rclcpp.hpp"

int main(int argc, char *argv[]) {
  // Initialize the ROS2 communication
  rclcpp::init(argc, argv);

  // Create a ROS2 node named ObiWan
  auto node = rclcpp::Node::make_shared("ObiWan");

  // We create a Rate object of 2Hz
  rclcpp::WallRate loop_rate(2);

  while (rclcpp::ok()) {
    RCLCPP_INFO(node->get_logger(),
                "HElp me Obi-Wan Kenobi, you're my only hope");
  rclcpp:
    spin_some(node);
    // Sleep the needed time to maintain the Rate fixed above
    loop_rate.sleep();
  }
  rclcpp::shutdown();
  return 0;
}