#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"
#include <cmath>
#include <functional>
#include <memory>
#include <thread>

#include "actions_quiz_msg/action/distance.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "std_msgs/msg/float64.hpp"

class MyActionServer : public rclcpp::Node {
public:
  using Distance = actions_quiz_msg::action::Distance;
  using GoalHandleMove = rclcpp_action::ServerGoalHandle<Distance>;

  explicit MyActionServer(
      const rclcpp::NodeOptions &options = rclcpp::NodeOptions())
      : Node("total_distance", options) {
    using namespace std::placeholders;

    this->action_server_ = rclcpp_action::create_server<Distance>(
        this, "distance_as",
        std::bind(&MyActionServer::handle_goal, this, _1, _2),
        std::bind(&MyActionServer::handle_cancel, this, _1),
        std::bind(&MyActionServer::handle_accepted, this, _1));

    publisher_ =
        this->create_publisher<std_msgs::msg::Float64>("total_distance", 10);
    subscription_ = this->create_subscription<nav_msgs::msg::Odometry>(
        "odom", 1000, std::bind(&MyActionServer::odom_callback, this, _1));
  }

private:
  rclcpp_action::Server<Distance>::SharedPtr action_server_;
  rclcpp::Publisher<std_msgs::msg::Float64>::SharedPtr publisher_;
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subscription_;

  float distance_total = 0.0;
  float distance_current = 0.0;
  float current_pos_x = 0.0;
  float current_pos_y = 0.0;
  float start_pos_x = 0.0;
  float start_pos_y = 0.0;

  void odom_callback(const nav_msgs::msg::Odometry::SharedPtr msg) {

    auto position = msg->pose.pose.position;
    this->current_pos_x = position.x;
    this->current_pos_y = position.y;
  }

  rclcpp_action::GoalResponse
  handle_goal(const rclcpp_action::GoalUUID &uuid,
              std::shared_ptr<const Distance::Goal> goal) {
    RCLCPP_INFO(this->get_logger(), "Received goal request with secs %i",
                goal->seconds);
    (void)uuid;
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
  }

  rclcpp_action::CancelResponse
  handle_cancel(const std::shared_ptr<GoalHandleMove> goal_handle) {
    RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
  }

  void handle_accepted(const std::shared_ptr<GoalHandleMove> goal_handle) {
    using namespace std::placeholders;
    // this needs to return quickly to avoid blocking the executor, so spin up a
    // new thread
    std::thread{std::bind(&MyActionServer::execute, this, _1), goal_handle}
        .detach();
  }

  float calculate_current_distance() {

    float distance_current =
        sqrt(pow(this->current_pos_x - this->start_pos_x, 2) +
             pow(this->current_pos_y - this->start_pos_y, 2));
    // float distance_travelled_rounded = float(int(float(distance_travelled) *
    // 1000.0) / 1000.0);
    float distance_current_rounded = round_3_decimals(distance_current);
    return distance_current_rounded;
  }

  // https://get-help.robotigniteacademy.com/t/quiz-6-5-action-quiz-distance-didnt-compute-correctly/25223
  float round_3_decimals(float number) {
    float A = number * 1000;
    // RCLCPP_INFO(this->get_logger(), "number*1000 = %f",A);
    int B = A;
    float C = B;
    // RCLCPP_INFO(this->get_logger(), "int then float step = %f",C);
    float rounded = C / 1000;
    return rounded;
  }

  void execute(const std::shared_ptr<GoalHandleMove> goal_handle) {
    RCLCPP_INFO(this->get_logger(), "Executing goal");
    const auto goal = goal_handle->get_goal();

    // const nav_msgs::msg::Odometry::SharedPtr odometry;

    auto feedback = std::make_shared<Distance::Feedback>();
    auto &fb_message = feedback->current_dist;
    fb_message = 0.0;
    auto result = std::make_shared<Distance::Result>();
    std_msgs::msg::Float64 distance_in_f64;
    rclcpp::Rate loop_rate(1);

    this->start_pos_x = this->current_pos_x;
    this->start_pos_y = this->current_pos_y;

    for (int i = 0; (i < goal->seconds) && rclcpp::ok(); ++i) {
      // Check if there is a cancel request
      if (goal_handle->is_canceling()) {
        result->status = false;
        result->total_dist = distance_current;
        goal_handle->canceled(result);
        RCLCPP_INFO(this->get_logger(), "Goal canceled");
        return;
      }
      // Move robot forward and send feedback
      distance_total += calculate_current_distance();
      this->start_pos_x = this->current_pos_x;
      this->start_pos_y = this->current_pos_y;

      fb_message = distance_total;
      distance_in_f64.data = distance_total;

      publisher_->publish(distance_in_f64);
      goal_handle->publish_feedback(feedback);
      RCLCPP_INFO(this->get_logger(), "Publish feedback");

      loop_rate.sleep();
    }

    // Check if goal is done
    if (rclcpp::ok()) {
      result->status = true;
      result->total_dist = distance_total;
      goal_handle->succeed(result);
      RCLCPP_INFO(this->get_logger(), "Goal succeeded");
    }
  }

}; // class MyActionServer

int main(int argc, char **argv) {
  rclcpp::init(argc, argv);

  auto action_server = std::make_shared<MyActionServer>();

  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(action_server);
  executor.spin();

  rclcpp::shutdown();
  return 0;
}