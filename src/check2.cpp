#include "custom_interfaces/action/odom_record.hpp"
#include "custom_interfaces/srv/find_wall.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"

#include "rclcpp_action/rclcpp_action.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <chrono>
#include <future>
#include <memory>
#include <ostream>
#include <stdio.h>
#include <thread>

using FindWallMessage = custom_interfaces::srv::FindWall;
using std::placeholders::_1;
using namespace std::chrono_literals;

class RobotRun : public rclcpp::Node {
public:
  float vx;
  float vz;

  using OdomRecord = custom_interfaces::action::OdomRecord;
  using GoalHandleOdomRecord = rclcpp_action::ClientGoalHandle<OdomRecord>;

  // Publisher node

  explicit RobotRun(
      const rclcpp::NodeOptions &node_options = rclcpp::NodeOptions())
      : Node("move_robot_service_action_node", node_options),
        goal_done_(false) {

    // Initialize the MutuallyExclusive callback group object
    //callback_publisher_robotrun_group_ = this->create_callback_group(
     //   rclcpp::CallbackGroupType::MutuallyExclusive);

    callback_action_robotrun_group_ = this->create_callback_group(
        rclcpp::CallbackGroupType::MutuallyExclusive);

    callback_subscriber_robotrun_group_ = this->create_callback_group(
        rclcpp::CallbackGroupType::MutuallyExclusive);

    rclcpp::SubscriptionOptions options1;

    options1.callback_group = callback_subscriber_robotrun_group_;

    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);

    subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "scan", 10, std::bind(&RobotRun::laser_callback, this, _1), options1);

    //timer_ = this->create_wall_timer(500ms,
     //                                std::bind(&RobotRun::timer_callback, this),
     //                                callback_publisher_robotrun_group_);

    this->client_ptr_ = rclcpp_action::create_client<OdomRecord>(
        this->get_node_base_interface(), this->get_node_graph_interface(),
        this->get_node_logging_interface(),
        this->get_node_waitables_interface(), "record_odom_as");

    this->timer_action_ = this->create_wall_timer(
        std::chrono::milliseconds(500), std::bind(&RobotRun::send_goal, this),
        callback_action_robotrun_group_);
  }

  bool is_goal_done() const 
  { 
    return this->goal_done_; 
  }

  void send_goal() {
    using namespace std::placeholders;


    // Cancel the timer so that it only gets executed one time 
    // Avoid  keep sending goals to the Action Server):
    this->timer_action_->cancel();

    this->goal_done_ = false;

    if (!this->client_ptr_) {
      RCLCPP_ERROR(this->get_logger(), "Action client not initialized");
    }
    //wait for the Action Server to start for 10 seconds. If it is not ready after the 10 seconds pass.
    if (!this->client_ptr_->wait_for_action_server(std::chrono::seconds(10))) {
      RCLCPP_ERROR(this->get_logger(),
                   "Action server not available after waiting");
      this->goal_done_ = true;
      return;
    }

    auto goal_msg = OdomRecord::Goal();

    RCLCPP_INFO(this->get_logger(), "Sending goal");

    auto send_goal_options =
        rclcpp_action::Client<OdomRecord>::SendGoalOptions();

    send_goal_options.goal_response_callback =
        std::bind(&RobotRun::goal_response_callback, this, _1);

    send_goal_options.feedback_callback =
        std::bind(&RobotRun::feedback_callback, this, _1, _2);

    send_goal_options.result_callback =
        std::bind(&RobotRun::result_callback, this, _1);

    auto goal_handle_future =
        this->client_ptr_->async_send_goal(goal_msg, send_goal_options);
  }

private:
rclcpp::TimerBase::SharedPtr timer_action_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
  rclcpp_action::Client<OdomRecord>::SharedPtr client_ptr_;
  bool goal_done_;
  rclcpp::CallbackGroup::SharedPtr callback_action_robotrun_group_;
  rclcpp::CallbackGroup::SharedPtr callback_subscriber_robotrun_group_;
  bool goal_response = false;


  void laser_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg) {
    auto message = geometry_msgs::msg::Twist();

    std::vector<float> ranges = msg->ranges;
    auto ranges_size = ranges.size();
    auto ranges_region = ranges_size / 10; // 360/36

    int turn = 0;
    double obstacle_threshold = 0.5;
    double too_far_threshold = 0.3;
    double too_close_threshold = 0.2;

    // message.linear.x = 0.1;
    // message.angular.z = 0.0;
    if (goal_response == true) {
      // Front
      for (size_t i = 1; i < ranges_region / 2; ++i) {
        auto L_mid_range = ranges[i];
        auto R_mid_range = ranges[360 - i];

        if ((std::isinf(L_mid_range) || std::isnan(L_mid_range)) ||
            (std::isinf(R_mid_range) || std::isnan(R_mid_range))) {
          // std::cout << " unknown value, continue " << i << std::endl;
          continue;
        }

        if ((L_mid_range < obstacle_threshold) ||
            (R_mid_range < obstacle_threshold)) {
          // closer turn faster
          // std::cout << " Obstacles detected, Turn Left immediately " << i
          //          << std::endl;
          turn = 1;
          message.linear.x = 0.05;
          message.angular.z = 0.19;
          break;
        } else {
          for (size_t i = 0; i < ranges_region / 2; ++i) {
            // auto LHS_range_ = ranges[90 - i];
            auto RHS_range_start = ranges[270 + i];
            auto RHS_range_end = ranges[270 + ranges_region - i];

            if ((std::isinf(RHS_range_start) || std::isnan(RHS_range_start)) ||
                (std::isinf(RHS_range_end) || std::isnan(RHS_range_end))) {
              // std::cout << " unknown value, continue " << i << std::endl;
              continue;
            }

            if ((RHS_range_start > too_far_threshold) ||
                (RHS_range_end > too_far_threshold)) {
              // std::cout << " Too far away, Turn Right " << RHS_range_start
              //           << " - " << RHS_range_end << std::endl;
              turn = 1;
              message.linear.x = 0.05;
              message.angular.z = -0.19;
              break;
            } else if ((RHS_range_start < too_close_threshold) ||
                       (RHS_range_end < too_close_threshold)) {
              // std::cout << " Too close, Turn Left " << RHS_range_start << " -
              // "
              // << RHS_range_end << std::endl;
              turn = 1;
              message.linear.x = 0.05;
              message.angular.z = 0.19;
              break;
            } else {
              turn = 0;
              message.linear.x = 0.19;
              message.angular.z = 0.0;
              // std::cout << " Move forward " << RHS_range_start << " - "
              //          << RHS_range_end << std::endl;
            }
          }
        }
      }
      // std::cout << " Final Turn Command :" << turn << std::endl;
      publisher_->publish(message);
    }
  }

//  void timer_callback() {
  //  auto message = geometry_msgs::msg::Twist();
   // message.linear.x = 0.1;
    //message.angular.z = vz;
    //publisher_->publish(message);
 // }

  void goal_response_callback(
      std::shared_future<GoalHandleOdomRecord::SharedPtr> future) {
    // this is necessary because the system uses ros2 foxy e not humble
    auto goal_handle = future.get();
    if (!goal_handle) {
      RCLCPP_ERROR(this->get_logger(), "Goal was rejected by server");
    } else {
      goal_response = true;
      RCLCPP_INFO(this->get_logger(),
                  "Goal accepted by server, waiting for result %d",
                  goal_response);
    }
  }

  void feedback_callback(
      GoalHandleOdomRecord::SharedPtr,
      const std::shared_ptr<const OdomRecord::Feedback> feedback) {
    RCLCPP_INFO(this->get_logger(), "Feedback received: %f",
                feedback->current_total);
  }

  void result_callback(const GoalHandleOdomRecord::WrappedResult &result) {
    this->goal_done_ = true;
    switch (result.code) {
    case rclcpp_action::ResultCode::SUCCEEDED:
      break;
    case rclcpp_action::ResultCode::ABORTED:
      RCLCPP_ERROR(this->get_logger(), "Goal was aborted");
      return;
    case rclcpp_action::ResultCode::CANCELED:
      RCLCPP_ERROR(this->get_logger(), "Goal was canceled");
      return;
    default:
      RCLCPP_ERROR(this->get_logger(), "Unknown result code");
      return;
    }
    // possivelmente vou printar um a um

    for (int i = 0; i < result.result->list_of_odoms.size(); ++i) {
     // RCLCPP_INFO(this->get_logger(), "X: %f. Y: %f. Theta: %f",
     //             result.result->list_of_odoms[i].x,
     //           result.result->list_of_odoms[i].y,
     //             result.result->list_of_odoms[i].z);

     std::cout << "Dado salvo: " << i + 1 << std::endl;
     std::cout << "X: . " << result.result->list_of_odoms[i].x ;
     std::cout << "Y: . " << result.result->list_of_odoms[i].y ;
     std::cout << "THETA: . " << result.result->list_of_odoms[i].z<< std::endl;
    }
    auto result_message = geometry_msgs::msg::Twist();
     result_message.linear.x = 0.0;
     result_message.angular.z = 0.0;
     publisher_->publish(result_message);
    std::cout << "Done"<< std::endl ;
     rclcpp::shutdown();
  
  }

  //rclcpp::TimerBase::SharedPtr timer_;
  
 //rclcpp::CallbackGroup::SharedPtr callback_publisher_robotrun_group_;

}; // End Node

class ServiceFW_Client : public rclcpp::Node {
public:
  ServiceFW_Client() : Node("find_wall_node") {
    client_ = this->create_client<FindWallMessage>("find_wall");

    timer_client_ = this->create_wall_timer(
        1s, std::bind(&ServiceFW_Client::timer_client_callback, this));
  }

  bool is_service_done() const { return this->service_done_; }

private:
  rclcpp::Client<FindWallMessage>::SharedPtr client_;
  rclcpp::TimerBase::SharedPtr timer_client_;
  bool service_done_ = false;

  void timer_client_callback() {
    while (!client_->wait_for_service(2s)) {
      if (!rclcpp::ok()) {
        RCLCPP_ERROR(
            this->get_logger(),
            "Client interrupted while waiting for service. Terminating...");
        return;
      }
      RCLCPP_INFO(this->get_logger(),
                  "Service Unavailable. Waiting for Service...");
    }
    // std::cout << "check _ " << check_ << std::endl;

    auto request = std::make_shared<FindWallMessage::Request>();

    service_done_ = false;
    auto result_future = client_->async_send_request(
        request, std::bind(&ServiceFW_Client::response_client_callback, this,
                           std::placeholders::_1));
  }

  void response_client_callback(
      rclcpp::Client<FindWallMessage>::SharedFuture future) {
    auto status = future.wait_for(1s);
    auto result = future.get();

    if (status == std::future_status::ready) {
      if (result->wallfound == true) {
        RCLCPP_INFO(this->get_logger(), "Service returned success");
        service_done_ = true;

      } else if (result->wallfound == false) {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned false");
      } else {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"),
                     "Failed to call service /moving");
      }
    } else {
      RCLCPP_INFO(this->get_logger(), "Service In-Progress...");
    }
  }
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);

  std::shared_ptr<RobotRun> run_robot_service_node =
      std::make_shared<RobotRun>();

  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(run_robot_service_node);
  executor.spin();

  rclcpp::shutdown();
  return 0;
}
