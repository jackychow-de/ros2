
#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <chrono>
#include <stdio.h>

using std::placeholders::_1;
using namespace std::chrono_literals;

class RunRobot : public rclcpp::Node {
public:
  // Publisher node
  RunRobot() : Node("topics_quiz_node") {
    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
    // timer_ =
    //    this->create_wall_timer(500ms, std::bind(&RunRobot::timer_callback,
    //    this));
    subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "scan", 10, std::bind(&RunRobot::laser_callback, this, _1));
  }

private:
  void laser_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg) {
    std::vector<float> ranges = msg->ranges;

    auto ranges_size = ranges.size();
    auto mid_point = ranges_size / 2;
    int turn = 0;
    int counter_L = 0;
    int counter_R = 0;

    if (ranges[mid_point] >= 1) {
      turn = 0;
      std::cout << " Move Forward " << std::endl;
    } else {
      turn = 1;
      std::cout << " Turn left obstacles detected " << std::endl;
    }

    if (turn == 0) {
      for (size_t i = 0; i < ranges.size(); ++i) {
        auto range_ = ranges[i];
        // if (std::isinf(range_)) {
        //   range_ = 0;
        // }

        if (range_ < 1) {
          if (i >= mid_point) {
            turn = 1;
            counter_L++;
            std::cout << " Turn Left ,i =" << i << std::endl;
          } else {
            turn = 2;
            counter_R++;
            std::cout << " Turn Right, i =" << i << std::endl;
          }
        }

        if (counter_L > 1) {
          turn = 1;
          break;
        }

        if (counter_R > 1) {
          turn = 2;
          break;
        }
      }
    }
    check_distance(turn);
  }

  void check_distance(int &turn) {
    auto message = geometry_msgs::msg::Twist();

    message.linear.x = 0.0;
    message.angular.z = 0.0;
    std::cout << "Get turn =" << turn << std::endl;

    switch (turn) {
    case 0: // Move Forward
      message.linear.x = 3;
      message.angular.z = 0.0;
      break;
    case 1: // Turn Left
      message.linear.x = 0.0;
      message.angular.z = 0.5;
      break;
    case 2: // Turn Right
      message.linear.x = 0.0;
      message.angular.z = -0.5;
      break;
    }
    /*
        if (turn == 0) {
          message.linear.x = 0.2;}

        if (turn == 1) {
            message.angular.z = 0.4;
          };

        if (turn == 2) {
            message.angular.z = -0.4;
          };*/
    publisher_->publish(message);
  }

  rclcpp::TimerBase::SharedPtr timer_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<RunRobot>());
  rclcpp::shutdown();
  return 0;
}