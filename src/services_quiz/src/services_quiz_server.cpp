#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "services_quiz_srv/srv/spin.hpp"
#include <chrono>
#include <memory>

using Spin = services_quiz_srv::srv::Spin;
using std::placeholders::_1;
using std::placeholders::_2;

class ServerNode : public rclcpp::Node {
public:
  ServerNode() : Node("rotate_server") {

    srv_ = create_service<Spin>(
        "rotate", std::bind(&ServerNode::rotate_callback, this, _1, _2));
    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
  }

private:
  rclcpp::Service<Spin>::SharedPtr srv_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;

  void rotate_callback(const std::shared_ptr<Spin::Request> request,
                       const std::shared_ptr<Spin::Response> response) {

    auto message = geometry_msgs::msg::Twist();

    auto angular_vel = request->angular_velocity;

    auto spin_time = request->time;

    rclcpp::Time start_time = rclcpp::Node::now();
    double start_time_s = start_time.seconds();

    if (request->direction == "right") {
      // Send velocities to move the robot to the right
      while (rclcpp::Node::now().seconds() - start_time_s < spin_time) {
        message.angular.z = -1 * angular_vel;
        publisher_->publish(message);
        rclcpp::sleep_for(std::chrono::milliseconds(100));
      }
      message.angular.z = 0;
      publisher_->publish(message);
      // Set the response success variable to true
      response->success = true;
    } else if (request->direction == "left") {
      // Send velocities to stop the robot
      while (rclcpp::Node::now().seconds() - start_time_s < spin_time) {
        message.angular.z = 1 * angular_vel;
        publisher_->publish(message);
        rclcpp::sleep_for(std::chrono::milliseconds(100));
      }
      message.angular.z = 0;
      publisher_->publish(message);
      // Set the response success variable to false
      response->success = true;
    } else {
      response->success = false;
    }
  }
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ServerNode>());
  rclcpp::shutdown();
  return 0;
}