#include "custom_interfaces/srv/find_wall.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp/utilities.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"

#include <memory>

using FindWallMessage = custom_interfaces::srv::FindWall;
using std::placeholders::_1;
using std::placeholders::_2;

class ServerNode : public rclcpp::Node {
public:
  ServerNode() : Node("find_wall_node") {

    srv_callback_group_ = create_callback_group(
        rclcpp::CallbackGroupType::MutuallyExclusive);

    scan_callback_group_ = create_callback_group(
        rclcpp::CallbackGroupType::MutuallyExclusive);

    rclcpp::SubscriptionOptions options1;
    options1.callback_group = scan_callback_group_;

    srv_ = this->create_service<FindWallMessage>(
        "find_wall", std::bind(&ServerNode::find_wall_callback, this, _1, _2),rmw_qos_profile_services_default, srv_callback_group_);
    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
    subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "scan", 1000, std::bind(&ServerNode::laser_callback, this, _1), options1);
 
   
  }

private:
  rclcpp::Service<FindWallMessage>::SharedPtr srv_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
  rclcpp::CallbackGroup::SharedPtr srv_callback_group_;
  rclcpp::CallbackGroup::SharedPtr scan_callback_group_;  

  enum RunningState {
    Initializing = -1,
    WallFinding = 0,
    MoveToWall = 1,
    Repositioning = 2,
    Completed = 3
  };

  RunningState current_state = Initializing;
 // current_state = Initializing;
  int min_ind;

  void find_wall_callback(
      const std::shared_ptr<FindWallMessage::Request> request,
      const std::shared_ptr<FindWallMessage::Response> response) {
    std::cout << "Request obtained.. Initalizing.." << std::endl;

    current_state = WallFinding;
    std::cout << "Finding a wall nearby... " << std::endl;

    while (rclcpp::ok() && current_state != Completed) {
      // rclcpp::spin_some();
      rclcpp::sleep_for(std::chrono::milliseconds(100));
    }

    // finalized
    if (current_state == Completed) {
      std::cout << "Wall finding process is completed -------------- " << std::endl;
      response->wallfound = true;
    } else {
      response->wallfound = false;
    }

  } // End find_wall_callback

  void laser_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg) {

    auto message = geometry_msgs::msg::Twist();
    std::vector<float> ranges = msg->ranges;
    // Inserting the value 500 at position 1(0-based
    // indexing) in the vector
    // vector_name.insert(vector_name.begin() + 1, 500);

    ranges.clear();
    ranges.insert(begin(ranges), begin(msg->ranges), end(msg->ranges));

    // 1st state : 1st rotate to point forward the wall
    if (current_state == WallFinding) {
      // Assume the distance between the robot and the wall is the shortest
      // Get the index of minimum in scanRanges
      // Rotate itself until the poitn it detected the shortest ray
      // 355..365 is minimum
      min_ind = std::min_element(ranges.begin(), ranges.end()) - ranges.begin();

      std::cout << "finding wall ... min index: " << min_ind << std::endl;

      if (!((min_ind > 355) && (min_ind < 365))) {
        message.angular.z = +0.15;
        publisher_->publish(message);
        // ros::spinOnce();
        // usleep(100);
      } else {
        message.angular.z = +0.0;
        publisher_->publish(message);
        // ros::spinOnce();
        // usleep(100);
        current_state = MoveToWall ;
      }
    }

    // 2nd state : Move towards the wall
    if (current_state == MoveToWall ) {
      std::cout << "Move to wall ... " << std::endl;
      if (ranges[360] > 0.3) {
        message.linear.x = +0.03;
        publisher_->publish(message);
        // ros::spinOnce();
        // usleep(100);
      } else {
        message.linear.x = +0.0;
        publisher_->publish(message);
        //   ros::spinOnce();
        //   usleep(100);
        current_state = Repositioning;
      }
    }

    // 3rd state : Reposition to the right direction
    if (current_state == Repositioning) {
      min_ind = std::min_element(ranges.begin(), ranges.end()) - ranges.begin();

      std::cout << "Repositioning for wall-following ... min index: " << min_ind
                << std::endl;
      if (!((min_ind > 265) && (min_ind < 275))) {
        message.angular.z = +0.15;
        publisher_->publish(message);
        //   ros::spinOnce();
        //   usleep(100);
      } else {
        message.angular.z = +0.0;
        publisher_->publish(message);
        // ros::spinOnce();
        // usleep(100);
        current_state = Completed;
      }
    }
  }
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);

  std::shared_ptr<ServerNode> find_wall_node =
      std::make_shared<ServerNode>();


  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(find_wall_node);
  executor.spin();

  rclcpp::shutdown();
  return 0;
}