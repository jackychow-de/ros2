
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='find_wall',
            executable='find_wall_server_node',
            output='screen'),
    ])