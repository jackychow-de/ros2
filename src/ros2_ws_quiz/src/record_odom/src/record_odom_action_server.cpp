#include <functional>
#include <memory>
#include <thread>

#include "geometry_msgs/msg/detail/point32__struct.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "custom_interfaces/action/odom_record.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "std_msgs/msg/float32.hpp"

class OdomActionServer : public rclcpp::Node
{
public:

   geometry_msgs::msg::Point32 odom_pt; // how do we get the point32 
    using OdomRecord = custom_interfaces::action::OdomRecord;
    using GoalHandleMove = rclcpp_action::ServerGoalHandle<OdomRecord>;
 
    explicit OdomActionServer(const rclcpp::NodeOptions & options = rclcpp::NodeOptions())
    : Node("record_odom", options){
    using namespace std::placeholders;

    this->action_server_ = rclcpp_action::create_server<OdomRecord>(
      this,
      "record_odom_as",
      std::bind(&OdomActionServer::handle_goal, this, _1),
      std::bind(&OdomActionServer::handle_cancel, this, _1),
      std::bind(&OdomActionServer::handle_accepted, this, _1));

    publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
    
    subscription_ = this->create_subscription<nav_msgs::msg::Odometry>(
        "odom", 10, std::bind(&OdomActionServer::odom_callback, this,_1));

  }

private:
  rclcpp_action::Server<OdomRecord>::SharedPtr action_server_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subscription_;
 
    float distance_total = 0.0; 
    float distance_current = 0.0;
    float current_pos_x = 0.0;
    float current_pos_y = 0.0;
    float start_pos_x = 0.0;
    float start_pos_y = 0.0;
    std::vector<geometry_msgs::msg::Point32> odom_point_list;

 // auto Point32 = 

    //How do we make empty goal?
  rclcpp_action::GoalResponse handle_goal(const rclcpp_action::GoalUUID & uuid)
   {
   RCLCPP_INFO(this->get_logger(), "Received goal: Record Odometry");
    (void)uuid;
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse handle_cancel(const std::shared_ptr<GoalHandleMove> goal_handle) 
    {
    RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
    }
 
    void handle_accepted(const std::shared_ptr<GoalHandleMove> goal_handle)
    {
    using namespace std::placeholders;
    // this needs to return quickly to avoid blocking the executor, so spin up a new thread
    std::thread{std::bind(&OdomActionServer::execute, this, _1), goal_handle}.detach();
    }

    float calculate_current_distance()
    {
    float distance_current =
        sqrt(pow(this->odom_pt.x - this->start_pos_x, 2) +
             pow(this->odom_pt.y - this->start_pos_y, 2));


      return distance_current;
    }

  void execute(const std::shared_ptr<GoalHandleMove> goal_handle)
  {
    RCLCPP_INFO(this->get_logger(), "Executing goal");
    
    const auto goal = goal_handle->get_goal();
    auto feedback = std::make_shared<OdomRecord::Feedback>();
    auto &fb_message = feedback->current_total;
    fb_message = 0.0;
    auto result = std::make_shared<OdomRecord::Result>();
   // std_msgs::msg::Float32 distance_in_f32;
    rclcpp::Rate loop_rate(1);
   
    this->start_pos_x = this->odom_pt.x;
    this->start_pos_y = this->odom_pt.y;

    distance_total += calculate_current_distance();


      fb_message = distance_total;
      //distance_in_f32.data = distance_total;
        

      //publisher_->publish(distance_in_f32);
      goal_handle->publish_feedback(feedback);
      RCLCPP_INFO(this->get_logger(), "Publish feedback");

      odom_point_list.push_back(this->odom_pt);
      
      loop_rate.sleep();
    
    // Check if goal is done
    if (rclcpp::ok()) {
      result->list_of_odoms = odom_point_list;
      goal_handle->succeed(result);
      RCLCPP_INFO(this->get_logger(), "Goal succeeded");
    }
  }


  
  void odom_callback(const nav_msgs::msg::Odometry::SharedPtr msg) {

    RCLCPP_INFO(this->get_logger(), "Odometry=['%f','%f','%f']",
                msg->pose.pose.position.x, msg->pose.pose.position.y,
                msg->pose.pose.position.z);

           this->odom_pt.x = msg->pose.pose.position.x;
           this->odom_pt.y  = msg->pose.pose.position.y;
           this->odom_pt.z  = msg->pose.pose.position.z;

    }  
};  // class OdomActionServer

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);

  auto action_server = std::make_shared<OdomActionServer>();
    
  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(action_server);
  executor.spin();

  rclcpp::shutdown();
  return 0;
}