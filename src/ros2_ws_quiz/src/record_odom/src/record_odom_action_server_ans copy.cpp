#include <functional>
#include <memory>
#include <thread>

#include "geometry_msgs/msg/detail/point32__struct.hpp"
#include "geometry_msgs/msg/detail/quaternion__struct.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "custom_interfaces/action/odom_record.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "std_msgs/msg/float32.hpp"

class OdomActionServer : public rclcpp::Node
{
public:

   geometry_msgs::msg::Point32 odom_pt; // how do we get the point32 
    using OdomRecord = custom_interfaces::action::OdomRecord;
    using GoalHandleMove = rclcpp_action::ServerGoalHandle<OdomRecord>;
 
    explicit OdomActionServer(const rclcpp::NodeOptions & options = rclcpp::NodeOptions())
    : Node("record_odom", options){
    using namespace std::placeholders;

    this->action_server_ = rclcpp_action::create_server<OdomRecord>(
      this,
      "record_odom_as",
      std::bind(&OdomActionServer::handle_goal, this, _1),
      std::bind(&OdomActionServer::handle_cancel, this, _1),
      std::bind(&OdomActionServer::handle_accepted, this, _1));

    publisher_ = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
    
    subscription_ = this->create_subscription<nav_msgs::msg::Odometry>(
        "odom", 10, std::bind(&OdomActionServer::odom_callback, this,_1));

  }

private:
  rclcpp_action::Server<OdomRecord>::SharedPtr action_server_;
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr subscription_;
  geometry_msgs::msg::Point32 actual_coords;
  std::vector<geometry_msgs::msg::Point32> coords;
 std::vector<geometry_msgs::msg::Point32> odom_point_list;

    float distance_total; 
    float distance_current;
    float current_pos_x = 0.0;
    float current_pos_y = 0.0;
    float start_pos_x = 0.0;
    float start_pos_y = 0.0;
    ;

   geometry_msgs::msg::Quaternion q;
   
   struct EulerAngles{
   
    float roll, pitch, yaw;
   };


    //How do we make empty goal?
  rclcpp_action::GoalResponse handle_goal(const rclcpp_action::GoalUUID & uuid)
   {
   RCLCPP_INFO(this->get_logger(), "Received goal: Record Odometry");
    (void)uuid;
    distance_total = 0.0; 
    distance_current = 0.0;
    return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }

    rclcpp_action::CancelResponse handle_cancel(const std::shared_ptr<GoalHandleMove> goal_handle) 
    {
    RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
    (void)goal_handle;
    return rclcpp_action::CancelResponse::ACCEPT;
    }
 
    void handle_accepted(const std::shared_ptr<GoalHandleMove> goal_handle)
    {
    using namespace std::placeholders;
    // this needs to return quickly to avoid blocking the executor, so spin up a new thread
    std::thread{std::bind(&OdomActionServer::execute, this, _1), goal_handle}.detach();
    }

    float calculate_current_distance(float &start_pos_x, float &start_pos_y, float &current_pos_x, float &current_pos_y)
    {
    return  sqrt(pow(this->current_pos_x - this->start_pos_x, 2) +
             pow(this->current_pos_y - this->start_pos_y, 2));
    }

  EulerAngles ToEulerAngles(geometry_msgs::msg::Quaternion &q){
  EulerAngles e_angles;
  
  //roll (x-axis rotation)
  double sinr_cosp = 2 *(q.w * q.x + q.y * q.z);
  double cosr_cosp = 1 - 2 *(q.x * q.x + q.y * q.y);
  angles.roll = std::atan2(sinr_cosp, cosr_cosp);

   //pitch (y-axis rotation)
  double sinp = std::sqrt(1 + 2 *(q.w * q.y - q.x * q.z));
  double cosp = std::sqrt(1 - 2 *(q.w * q.y - q.x * q.z));
  angles.pitch = std::atan2(sinp, cosp) - M_PI /2;
  
  //yaw (z-axis rotation)
  double siny_cosp = 2 *(q.w * q.x + q.x * q.y);
  double cosy_cosp = 1 - 2 *(q.y * q.y + q.z * q.z);
  angles.yaw = std::atan2(siny_cosp, cosy_cosp);
      
   return angles;
  }

  void execute(const std::shared_ptr<GoalHandleMove> goal_handle)
  {
    RCLCPP_INFO(this->get_logger(), "Executing goal");
    
    const auto goal = goal_handle->get_goal();
    auto feedback = std::make_shared<OdomRecord::Feedback>();
    auto &fb_message = feedback->current_total;
    fb_message = distance;

    auto result = std::make_shared<OdomRecord::Result>();
   // std_msgs::msg::Float32 distance_in_f32;
    //rclcpp::Rate loop_rate(1);
    bool lap = false;
    bool ExitFromStart = false; 

    While (lap == false && rclcpp::ok()){
      
    if (goal_handle->is_canceling()){  
      result->list_of_odoms = coords;
      goal_handle->canceled(result);
      RCLCPP_INFO(this->get_logger(), "Goal canceled");
    }
    
    EulerAngles angles = ToEulerAngles(q);
    actual_coords.set__z(angles.yaw  *180/ 3.1415);

    coords.push_back(actual_coords);

    if (coords.size()== 1){
        delta = 0;
    }else{
      delta = calculate_current_distance(
      coords[coords.size() - 2].x, coords[coords.size() - 2].y, 
      coords[coords.size() - 1].x, coords[coords.size() - 1].y
      );
    }

    distance = distance + delta;
    fb_message = distance;

    goal_handle->publish_feedback(feedback);

    DistanceFromStart = calculateDistance(coords[0].x, coords[0].y,coords[coords.size() - 1].x, coords[coords.size() - 1].y);

    if (DistanceFromStart > 0.5){ExitFromStart = true;}
    if (DistanceFromStart < 0.25 && ExitFromStart == true)
    {lap = true;
    std::cout<<"One lap is made."<<std::endl;}
    std::this_thread::sleep_for(1000ms);
    }
    
    this->start_pos_x = this->odom_pt.x;
    this->start_pos_y = this->odom_pt.y;

    distance_total += calculate_current_distance();


      fb_message = distance_total;
      //distance_in_f32.data = distance_total;
        

      //publisher_->publish(distance_in_f32);
      goal_handle->publish_feedback(feedback);
      RCLCPP_INFO(this->get_logger(), "Publish feedback");

      odom_point_list.push_back(this->odom_pt);
      
      loop_rate.sleep();
    
    // Check if goal is done
    if (rclcpp::ok()) {
      result->list_of_odoms = odom_point_list;
      goal_handle->succeed(result);
      RCLCPP_INFO(this->get_logger(), "Goal succeeded");
    }
  }


  
  void odom_callback(const nav_msgs::msg::Odometry::SharedPtr msg) {

    RCLCPP_INFO(this->get_logger(), "Odometry=['%f','%f','%f']",
                msg->pose.pose.position.x, msg->pose.pose.position.y,
                msg->pose.pose.position.z);

           this->q.x = msg->pose.pose.position.x;
           this->q.y  = msg->pose.pose.position.y;                                                                                                                                                                                                                                                                                  
           this->q.z  = msg->pose.pose.position.z;
     
           this->actual_coords.x = msg->pose.pose.position.x;
           this->actual_coords.y  = msg->pose.pose.position.y;                                                                                                                                                                                                                                                                                  
           this->actual_coords.z  = msg->pose.pose.position.z;                                                                                                                                                                                                                                                 

    }  
};  // class OdomActionServer

int main(int argc, char ** argv)
{
  rclcpp::init(argc, argv);

  auto action_server = std::make_shared<OdomActionServer>();
    
  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(action_server);
  executor.spin();

  rclcpp::shutdown();
  return 0;
}