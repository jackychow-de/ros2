
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='record_odom',
            executable='record_odom_action_server_node',
            output='screen'),
    ])