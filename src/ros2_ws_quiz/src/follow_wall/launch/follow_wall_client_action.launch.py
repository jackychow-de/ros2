
from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='follow_wall',
            executable='follow_wall_client_action_node',
            output='screen'),
    ])