#include "custom_interfaces/srv/find_wall.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <chrono>
#include <ostream>
#include <stdio.h>

using FindWallMessage = custom_interfaces::srv::FindWall;
using std::placeholders::_1;
using namespace std::chrono_literals;

class ClientNode : public rclcpp::Node {
public:
  // Publisher node
  ClientNode() : Node("follow_wall_node") {

    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);

    subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "scan", 1000, std::bind(&ClientNode::laser_callback, this, _1));
  }

  // bool is_service_done() const { return this->service_done_; }

private:
  void laser_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg) {
    auto message = geometry_msgs::msg::Twist();

    std::vector<float> ranges = msg->ranges;
    auto ranges_size = ranges.size();
    auto ranges_region = ranges_size / 10; // 360/36

    int turn = 0;
    double obstacle_threshold = 0.5;
    double too_far_threshold = 0.3;
    double too_close_threshold = 0.2;

    // message.linear.x = 0.1;
    // message.angular.z = 0.0;

    // Front
    for (size_t i = 1; i < ranges_region / 2; ++i) {
      auto L_mid_range = ranges[i];
      auto R_mid_range = ranges[360 - i];

      if ((std::isinf(L_mid_range) || std::isnan(L_mid_range)) ||
          (std::isinf(R_mid_range) || std::isnan(R_mid_range))) {
        std::cout << " unknown value, continue " << i << std::endl;
        continue;
      }

      if ((L_mid_range < obstacle_threshold) ||
          (R_mid_range < obstacle_threshold)) {
        // closer turn faster
        std::cout << " Obstacles detected, Turn Left immediately " << i
                  << std::endl;
        turn = 1;
        message.linear.x = 0.1;
        message.angular.z = 0.4;
        break;
      } else {
        for (size_t i = 0; i < ranges_region / 2; ++i) {
          // auto LHS_range_ = ranges[90 - i];
          auto RHS_range_start = ranges[270 + i];
          auto RHS_range_end = ranges[270 + ranges_region - i];

          if ((std::isinf(RHS_range_start) || std::isnan(RHS_range_start)) ||
              (std::isinf(RHS_range_end) || std::isnan(RHS_range_end))) {
            std::cout << " unknown value, continue " << i << std::endl;
            continue;
          }

          if ((RHS_range_start > too_far_threshold) ||
              (RHS_range_end > too_far_threshold)) {
            std::cout << " Too far away, Turn Right " << RHS_range_start
                      << " - " << RHS_range_end << std::endl;
            turn = 1;
            message.linear.x = 0.1;
            message.angular.z = -0.3;
            break;
          } else if ((RHS_range_start < too_close_threshold) ||
                     (RHS_range_end < too_close_threshold)) {
            std::cout << " Too close, Turn Left " << RHS_range_start << " - "
                      << RHS_range_end << std::endl;
            turn = 1;
            message.linear.x = 0.1;
            message.angular.z = 0.3;
            break;
          } else {
            turn = 0;
            message.linear.x = 0.1;
            message.angular.z = 0.0;
            std::cout << " Move forward " << RHS_range_start << " - "
                      << RHS_range_end << std::endl;
          }
        }
      }
    }
    std::cout << " Final Turn Command :" << turn << std::endl;
    publisher_->publish(message);
  }
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
}; // End Node

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  bool service_done_ = false;

  // if (service_done_ == false){
  std::shared_ptr<rclcpp::Node> node =
      rclcpp::Node::make_shared("find_wall_node");

  rclcpp::Client<FindWallMessage>::SharedPtr client =
      node->create_client<FindWallMessage>("find_wall");

  auto request = std::make_shared<FindWallMessage::Request>();

  while (!client->wait_for_service(1s)) {
    if (!rclcpp::ok()) {
      RCLCPP_ERROR(rclcpp::get_logger("rclcpp"),
                   "Interrupted while waiting for the service. Exiting.");
      return 0;
    }
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),
                "service not available, waiting again...");
  }

  // request = {};

  auto result_future = client->async_send_request(request);
  // Wait for the result.
  if (rclcpp::spin_until_future_complete(node, result_future) ==
      rclcpp::FutureReturnCode::SUCCESS) {
    auto result = result_future.get();

    if (result->wallfound == true) {
      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned success");

      rclcpp::spin(std::make_shared<ClientNode>());
      rclcpp::shutdown();

    } else if (result->wallfound == false) {
      RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned false");
    }
  } else {
    RCLCPP_ERROR(rclcpp::get_logger("rclcpp"),
                 "Failed to call service /moving");
  }

  rclcpp::shutdown();
  return 0;
}