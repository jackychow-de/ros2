#include "custom_interfaces/srv/find_wall.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <chrono>
#include <ostream>
#include <stdio.h>

using FindWallMessage = custom_interfaces::srv::FindWall;
using std::placeholders::_1;
using namespace std::chrono_literals;

class RobotRun : public rclcpp::Node {
public:
  // Publisher node
  RobotRun() : Node("follow_wall_node") {

    publisher_ =
        this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);

    subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "scan", 1000, std::bind(&RobotRun::laser_callback, this, _1));
  }

private:
  void laser_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg) {
    auto message = geometry_msgs::msg::Twist();

    std::vector<float> ranges = msg->ranges;
    auto ranges_size = ranges.size();
    auto ranges_region = ranges_size / 10; // 360/36

    int turn = 0;
    double obstacle_threshold = 0.5;
    double too_far_threshold = 0.3;
    double too_close_threshold = 0.2;

    // message.linear.x = 0.1;
    // message.angular.z = 0.0;

    // Front
    for (size_t i = 1; i < ranges_region / 2; ++i) {
      auto L_mid_range = ranges[i];
      auto R_mid_range = ranges[360 - i];

      if ((std::isinf(L_mid_range) || std::isnan(L_mid_range)) ||
          (std::isinf(R_mid_range) || std::isnan(R_mid_range))) {
        std::cout << " unknown value, continue " << i << std::endl;
        continue;
      }

      if ((L_mid_range < obstacle_threshold) ||
          (R_mid_range < obstacle_threshold)) {
        // closer turn faster
        std::cout << " Obstacles detected, Turn Left immediately " << i
                  << std::endl;
        turn = 1;
        message.linear.x = 0.1;
        message.angular.z = 0.4;
        break;
      } else {
        for (size_t i = 0; i < ranges_region / 2; ++i) {
          // auto LHS_range_ = ranges[90 - i];
          auto RHS_range_start = ranges[270 + i];
          auto RHS_range_end = ranges[270 + ranges_region - i];

          if ((std::isinf(RHS_range_start) || std::isnan(RHS_range_start)) ||
              (std::isinf(RHS_range_end) || std::isnan(RHS_range_end))) {
            std::cout << " unknown value, continue " << i << std::endl;
            continue;
          }

          if ((RHS_range_start > too_far_threshold) ||
              (RHS_range_end > too_far_threshold)) {
            std::cout << " Too far away, Turn Right " << RHS_range_start
                      << " - " << RHS_range_end << std::endl;
            turn = 1;
            message.linear.x = 0.1;
            message.angular.z = -0.3;
            break;
          } else if ((RHS_range_start < too_close_threshold) ||
                     (RHS_range_end < too_close_threshold)) {
            std::cout << " Too close, Turn Left " << RHS_range_start << " - "
                      << RHS_range_end << std::endl;
            turn = 1;
            message.linear.x = 0.1;
            message.angular.z = 0.3;
            break;
          } else {
            turn = 0;
            message.linear.x = 0.1;
            message.angular.z = 0.0;
            std::cout << " Move forward " << RHS_range_start << " - "
                      << RHS_range_end << std::endl;
          }
        }
      }
    }
    std::cout << " Final Turn Command :" << turn << std::endl;
    publisher_->publish(message);
  }
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr publisher_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
}; // End Node

class ServiceFW_Client : public rclcpp::Node {
public:
  ServiceFW_Client() : Node("find_wall_node") {
    client_ = this->create_client<FindWallMessage>("find_wall");

    timer_client_ = this->create_wall_timer(
        1s, std::bind(&ServiceFW_Client::timer_client_callback, this));
    
     
  }
  
  bool is_service_done() const { return this->service_done_; }

private:
  rclcpp::Client<FindWallMessage>::SharedPtr client_;
  rclcpp::TimerBase::SharedPtr timer_client_;
  bool service_done_ = false;


  void timer_client_callback() {
    while (!client_->wait_for_service(2s)) {
      if (!rclcpp::ok()) {
        RCLCPP_ERROR(
            this->get_logger(),
            "Client interrupted while waiting for service. Terminating...");
        return;
      }
      RCLCPP_INFO(this->get_logger(),
                  "Service Unavailable. Waiting for Service...");
    }
    //std::cout << "check _ " << check_ << std::endl;

    auto request = std::make_shared<FindWallMessage::Request>();

    service_done_ = false;
    auto result_future = client_->async_send_request(
        request, std::bind(&ServiceFW_Client::response_client_callback, this,
                           std::placeholders::_1));
     
  }

  void response_client_callback(
      rclcpp::Client<FindWallMessage>::SharedFuture future) {
    auto status = future.wait_for(1s);
    auto result = future.get();

    if (status == std::future_status::ready) {
      if (result->wallfound == true) {
        RCLCPP_INFO(this->get_logger(), "Service returned success");
        service_done_ = true;
        
      } else if (result->wallfound == false) {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned false");
      } else {
        RCLCPP_ERROR(rclcpp::get_logger("rclcpp"),
                     "Failed to call service /moving");
      }
    } else {
      RCLCPP_INFO(this->get_logger(), "Service In-Progress...");
    }
  }
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);

  auto service_client = std::make_shared<ServiceFW_Client>();
  
  if (service_client->is_service_done() == false) {
  while (!service_client->is_service_done()) {
    rclcpp::spin_some(service_client);
  }
}
  std::shared_ptr<RobotRun> run_robot_service_node =
      std::make_shared<RobotRun>();

  rclcpp::executors::MultiThreadedExecutor executor;
  executor.add_node(run_robot_service_node);
  executor.spin();

  rclcpp::shutdown();
  return 0;
}