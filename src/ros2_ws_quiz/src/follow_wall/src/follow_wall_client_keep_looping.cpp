#include "custom_interfaces/srv/find_wall.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <chrono>
#include <ostream>
#include <stdio.h>

using FindWallMessage = custom_interfaces::srv::FindWall;
using std::placeholders::_1;
using namespace std::chrono_literals;

class ClientNode : public rclcpp::Node {
public:
  // Publisher node
  ClientNode() : Node("follow_wall_node") {
    client_ = this->create_client<FindWallMessage>("find_wall");

    timer_ = this->create_wall_timer(
        1s, std::bind(&ClientNode::timer_callback, this));

    // publisher_ =
    //     this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);
  }

  bool is_service_done() const { return this->service_done_; }

private:
  rclcpp::Client<FindWallMessage>::SharedPtr client_;

  rclcpp::TimerBase::SharedPtr timer_;
  bool service_done_ = false;
  bool check = false;

  void timer_callback() {
    while (!client_->wait_for_service(1s)) {
      if (!rclcpp::ok()) {
        RCLCPP_ERROR(
            this->get_logger(),
            "Client interrupted while waiting for service. Terminating...");
        return;
      }
      RCLCPP_INFO(this->get_logger(),
                  "Service Unavailable. Waiting for Service...");
    }

    auto request = std::make_shared<FindWallMessage::Request>();

    if (check_ == false) {
      auto result_future = client_->async_send_request(request);

      if (rclcpp::spin_until_future_complete(std::make_shared<ClientNode>(),
                                             result_future) ==
          rclcpp::FutureReturnCode::SUCCESS) {

        auto result = result_future.get();

        if (result->wallfound == true) {
          service_done_ = true;
          RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned success");
        } else if (result->wallfound == false) {
          service_done_ = false;
          check_ == true;
          std::cout << "check _ " << check_ << std::endl;
          RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Service returned false");
        } else {
          RCLCPP_ERROR(rclcpp::get_logger("rclcpp"),
                       "Failed to call service /moving");
        }
      }
    }
  }
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto follow_wall_client_node = std::make_shared<ClientNode>();
  while (!follow_wall_client_node->is_service_done()) {
    rclcpp::spin_some(follow_wall_client_node);
  }
  rclcpp::shutdown();
  return 0;
}