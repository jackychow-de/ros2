#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <stdio.h>

using std::placeholders::_1;

class LaserSubscriber : public rclcpp::Node {
public:
  LaserSubscriber() : Node("sub_laser") {
    subscription_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
        "scan", 10, std::bind(&LaserSubscriber::laser_callback, this, _1));
  }

private:
  void laser_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg) {
    // RCLCPP_INFO(this->get_logger(), "Range max heard: '%f'", msg->range_max);
    // RCLCPP_INFO(this->get_logger(), "Ranges Size: '%ld'",
    // msg->ranges.size());
    std::vector<float> ranges = msg->ranges;
    // std::cout << ranges.size();

    auto ranges_size = ranges.size();
    std::cout << "Get Ranges'%ld': " << ranges_size << std::endl;

    // Print the ranges
    std::cout << "Middle Range value is :" << ranges[ranges_size / 2]
              << std::endl;
    // for (size_t i = 0; i < ranges.size(); ++i) {
    //<< std::endl;
    // std::cout << " " << ranges[360]<< std::endl;
    // printf(msg->range_min);
  }

  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr subscription_;
};

int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<LaserSubscriber>());
  rclcpp::shutdown();
  return 0;
}