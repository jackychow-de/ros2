# ROS2 Basics in 5 Days (C++)


## Description
These are work tasks in the online course held by the Construct
https://app.theconstructsim.com/


## Project status
- 95% completed. All execrises of each chapter are completed.
- Still have to refine the action server for controlling the TurtleBot3.

##Knowledge 
GoalResponseCallback
future vs goal_handle

ROS2 Foxy :
void goal_response_callback(std::shared_future<GoalHandleOdomRecord::SharedPtr> future)
{
    // ...
}
https://docs.ros.org/en/foxy/Tutorials/Intermediate/Writing-an-Action-Server-Client/Cpp.html

ROS2 Humble :
void goal_response_callback(const GoalHandleOdomRecord::SharedPtr & goal_handle)
{
    // ...
}
https://docs.ros.org/en/humble/Tutorials/Intermediate/Writing-an-Action-Server-Client/Cpp.html

Reference:
https://get-help.robotigniteacademy.com/t/error-compiling-send-goal-function-of-action-client-wallfollower-project/23051/7


===================================================
catkin_ws

ROS C++

C++ compile process  
- performs 4 major steps, namely: Pre-process, Compile, Assembly and Link.


1. Pre-process
clang++ -E hello.cpp > hello.i
.i -> (pre proccessed file)

adding the contents of the header files in the main code.
 definitions of functions and variables
 
 
iostream. It provides basic input and output services for C++ programs.

preprocessor is responsible for
 removing comments, 
 expanding macros (if any) and 
 expanding included files

----------------------------------------------- 

 2. Compile
   clang++ -S hello.i
 .s -> assembly language
 
  translate the preprocessed code to assembly code.

----------------------------------------------- 
 3. Assembly
  clang++ -c hello.s
  translate the assembly instructions to binary code 
  that the processor understands.
-----------------------------------------------   
 4. Link
  clang++ hello.o -o hello
  hello.s -> exe
  
  
  linker is responsible for 
  identifying all the dependencies and making sure everything is put together in a final executable file
  
  object files, library, cross reference files
  
First, it takes all the object files generated 
by the compiler and combines them into a single executable program.

Second, in addition to being able to link object files, 
the linker also is capable of linking library files. 

A library file is a collection of precompiled code that 
has been “packaged up” for reuse in other programs.

Third, the linker makes sure all cross-file dependencies are resolved properly.
 For example, if you define something in one .cpp file, 
 and then use it in another .cpp file, the linker connects the two.
 If the linker is unable to connect a reference to something with its definition,
 you’ll get a linker error, and the linking process will abort.

-----------------------------------------------   
Commands:  

-E: Run the preprocessor stage.

-S: Run the previous stages as well as LLVM generation (compiler technology) and optimization stages (optimize compilation process at the cost of compilation time) and target-specific code generation, producing an assembly file.

-c: Run all of the above, plus the assembler, generating a target “.o” object file.

-o <file>: Write output to file

std=<standard>: Specify the language standard to compile for.


================================================================================================
What is a library?
The library is a binary object that contains the compiled implementation of some methods.
Linking maps a function declaration to its compiled implementation

 A header file something.h and the compiled library object something.a.

when you are writing a programm to perform some physics simulations,
you write physics equation in a separate file. 

 increasing the usability, readability and ability to share

1. Static :
lib*.a
Pros: Faster, takes up more space (because a copy of static library becomes a part of every executable that uses it),

Cons: Cannot be upgraded easily, replace the entire executable needs

2. Dynamic :
lib*.so
 Slower, can be copied, referenced by a program.
 
 After compile, it remains a separate unit.
 
 upgraded to a newer version without replacing all the executables that use it.
 

 *ldconfig -p 
  show which libraries the dynamic linker keeps in cache 
  
  
================================================================================================
Declaration and Definition which are two basic concepts that are often used in the process of programming in C++.
 
 
1. Function Declaration (header file) .hpp

A declaration is a promise to the preprocessor to provide 
the variable or function definition.

For instance:

void LearningLibrary(int param);


2. Function Definition(Implementation file) .cpp  .cc

In the definition, you provide the preprocessor with the full-length code.

This is the definition corresponding to the above declaration:

void LearningLibrary(int param){
    // Implementation details
    cout<<"Just an example to show a function definition";
}

 and implementation file.
 
 ***Avoid the use of linker will cause the build process to be super slow or fail.
 
 **#pragma once is preprocessor directive designed to cause the current source file to be included only once in a single compilation.
 #################
 1st Library
 Compile Modules
 clang++ -std=c++17 -c first.cpp -o first.o
 
 Organize modules into libraries
 ar rcs libfirst.a first.o
 #################
 
-turns the object file first.o into a static library (archive) libfirst.a 
using the ar tools from the binutils package (GNU Binary Utilities, or 
binutils, are a set of programming tools for creating and managing binary programs, object files, libraries, profile data, and assembly source code).

In the command line argument, ar stands for archive and rcs stands for:

r means that if the library already exists, replace the old files within the library with your new files.
c means creating the library if it did not exist.
s means 'sort' (create a sorted index of) the library, so that it will be indexed and faster to access the functions in the library.
Therefore, rcs can be understood as replace, create, sort.

Link libraries when building code
clang++ -std=c++17 main.cpp -L . -lfirst -o main

The directories searched include several standard system directories plus any that you specify with the -L flag. 
Here . means present directory.
The -l option adds a library. -l=lib 

To run the code
./  

1.6   Build System
e.g. Make

 Build generator tool
 CMake - generates build files for Make from a simpler project configuration file


Create CMakeLists.txt
----------------------------------------------
#define the version of CMake you are using here 3.1
cmake_minimum_required(VERSION 3.1)
#project name
project(first_project)
#C++ version in use, here 17
set(CMAKE_CXX_STANDARD 17)

#tell CMake where to look for *.hpp ,*.h files
include_directories(include/)

#create library "libfirst", this creates libfirst.a 
add_library(first src/first.cpp)

#add executable main, this creates main.o
add_executable(main src/main.cpp)

#tell the linker to bind these object files together and provide with the final executable
target_link_libraries(main first)
----------------------------------------------

- Assume the .cpp, .hpp, main.cpp are created. 
 
 Command
 cd/src 
 cmake ..
 make
 
 set(CMAKE_EXPORT_COMPILE_COMMANDS ON) in the CMakeLists.txt file 
 has created the compile_commands.json
 
 =========================
 Library Package including the header in another ROS package
 
 cd ~/catkin_ws/src
 catkin_create_pkg test_library roscpp
 cd test_library
 touch src/test_library.cpp
 touch include/test_library/test_library.h
 
 
----------test_library.h
#ifndef TEST_LIBRARY_H
#define TEST_LIBRARY_H

#include <ros/ros.h>

void display_pos(float x, float y);

#endif

----------test_library.cpp
#include "../include/test_library/test_library.h"

void display_pos(float x, float y) {
     
    float pos_x = x;
    float pos_y = y;
    std::cout << "Position X: " << pos_x << " Position Y: " << pos_y << std::endl;
     
}

------------------CMakeLists.txt
cmake_minimum_required(VERSION 3.0.2)
project(test_library)


find_package(catkin REQUIRED COMPONENTS
  roscpp
)


catkin_package(
  INCLUDE_DIRS include
  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS roscpp
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
  src/${PROJECT_NAME}.cpp
)


## Specify libraries to link a library or executable target against
target_link_libraries(${PROJECT_NAME}
${catkin_LIBRARIES}
)

install(TARGETS ${PROJECT_NAME}
   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION}
)

## Mark cpp header files for installation
install(DIRECTORY include/${PROJECT_NAME}/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION})
   
   cd catkin_ws
   catkin_make  =  

Command   catkin_make  =  CMake + make

sets certain ENV variables before running the build generator and building process.]

 better than rosbuild in portability, cross-compiling support, distribution of packages
 
 
 ====================Example
 cd ~/catkin_ws/src
 catkin_create_pkg odom_subs nav_msgs rospy roscpp